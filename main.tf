terraform {

  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.23.0"
    }
  }
}

provider "google" {
 // project = "ticket75113-test"
  project = "hc-19ba1130a51d4229ae740fc2a20"
  credentials = var.gcp_credentials
  region = "us-central1"
  zone = "us-central1-a"
}

variable "gcp_credentials" {}


data "google_project" "project1" {
}

output "project_number" {
  value = data.google_project.project1.number
}
